package com.wjy.mdemo.mvp;

import com.wjy.mdemo.bean.UserInfo;

/**
 * created by WangJinyong
 * 提供视图功能的接口
 */
public interface IMVPView {

    /**
     * 获取用户输入的信息,直接返回输入框的信息就可以了
     */
    String getInputData();

    /**
     * 获取数据成功
     */
    void showSuccess(UserInfo userInfo);

    /**
     * 获取数据失败
     */
    void showFailed();
}
