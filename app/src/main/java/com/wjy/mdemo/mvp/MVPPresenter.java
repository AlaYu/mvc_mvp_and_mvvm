package com.wjy.mdemo.mvp;

import com.wjy.mdemo.bean.UserInfo;
import com.wjy.mdemo.callback.MCallBack;

/**
 * created by WangJinyong
 * 中间关联层   持有View和Model层的引用
 */
public class MVPPresenter {

    private IMVPView imvpView;
    private MVPModel mvpModel;

    public MVPPresenter(IMVPView imvpView) {
        this.imvpView = imvpView;
        mvpModel = new MVPModel();
    }

    //查询 获取数据
    public void getData(String inputName){
        mvpModel.getUserInfo(inputName, new MCallBack() {
            @Override
            public void onSuccess(UserInfo userInfo) {
                imvpView.showSuccess(userInfo);//成功
            }

            @Override
            public void onFailed() {
                imvpView.showFailed();//失败
            }
        });
    }
}
