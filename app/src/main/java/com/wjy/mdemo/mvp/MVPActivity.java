package com.wjy.mdemo.mvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wjy.mdemo.R;
import com.wjy.mdemo.bean.UserInfo;

/**
 * created by WangJinyong
 */
public class MVPActivity extends AppCompatActivity implements View.OnClickListener,IMVPView {

    private EditText et_name;//输入要查询的用户名
    private Button btn_search;//查询按钮
    private TextView tv_result;//显示查询结果
    private MVPPresenter mvpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvp);
        initView();
        mvpPresenter = new MVPPresenter(this);
    }

    //初始化View
    private void initView(){
        et_name = findViewById(R.id.et_name);
        btn_search = findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        tv_result = findViewById(R.id.tv_result);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search://查询用户信息
                mvpPresenter.getData(getInputData());
                break;
        }
    }

    @Override
    public String getInputData() {
        return et_name.getText().toString();
    }

    @Override
    public void showSuccess(UserInfo userInfo) {
        tv_result.setText("查询结果：\n用户名："+userInfo.getName()+"\n性别："+userInfo.getSex()+"\n年龄："+userInfo.getAge());
    }

    @Override
    public void showFailed() {
        tv_result.setText("查询结果：\n获取数据失败！");
    }
}
