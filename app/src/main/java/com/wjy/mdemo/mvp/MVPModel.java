package com.wjy.mdemo.mvp;

import com.wjy.mdemo.bean.UserInfo;
import com.wjy.mdemo.callback.MCallBack;

import java.util.Random;

/**
 * created by WangJinyong
 * 查询用户信息
 */
public class MVPModel {

    /**
     * 模拟 获取用户数据(先不通过网络或者数据库查询来获取数据了)
     */
    public void getUserInfo(String inputName, MCallBack mCallBack){
        Random random = new Random();
        boolean isSuccess = random.nextBoolean();//通过Random随机获取师傅成功
        if (isSuccess){
            //如果是成功，设置模拟数据
            UserInfo userInfo = new UserInfo();
            userInfo.setName("张三");
            userInfo.setSex("男");
            userInfo.setAge(25);
            mCallBack.onSuccess(userInfo);//调取成功方法，通知用户，并把模拟数据传递过去
        }else {
            //如果失败  调取失败方法，直接通知用户失败
            mCallBack.onFailed();
        }
    }
}
