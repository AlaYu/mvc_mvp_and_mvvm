package com.wjy.mdemo.callback;

import com.wjy.mdemo.bean.UserInfo;

/**
 * created by WangJinyong
 * 通知用户查询结果的接口
 */
public interface MCallBack {

    void onSuccess(UserInfo userInfo);//查询成功
    void onFailed();//查询失败
}
