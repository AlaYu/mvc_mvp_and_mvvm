package com.wjy.mdemo.bean;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.wjy.mdemo.BR;

/**
 * created by WangJinyong
 * 封装用户信息实体类
 */
public class UserInfo extends BaseObservable {

    private String name;//用户名
    private String sex;//性别
    private int age;//年龄

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Bindable
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
        notifyPropertyChanged(BR.age);
    }
}
