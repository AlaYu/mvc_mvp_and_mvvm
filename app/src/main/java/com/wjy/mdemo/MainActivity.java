package com.wjy.mdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.wjy.mdemo.databinding.DataBindingActivity;
import com.wjy.mdemo.mvc.MVCActivity;
import com.wjy.mdemo.mvp.MVPActivity;
import com.wjy.mdemo.mvvm.MVVMActivity;
import com.wjy.mdemo.normal.NormalActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_Normal,btn_mvc,btn_mvp,btn_databinding,btn_mvvm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    //初始化View
    private void initView(){
        btn_Normal = findViewById(R.id.btn_Normal);
        btn_Normal.setOnClickListener(this);
        btn_mvc = findViewById(R.id.btn_mvc);
        btn_mvc.setOnClickListener(this);
        btn_mvp = findViewById(R.id.btn_mvp);
        btn_mvp.setOnClickListener(this);
        btn_databinding = findViewById(R.id.btn_databinding);
        btn_databinding.setOnClickListener(this);
        btn_mvvm = findViewById(R.id.btn_mvvm);
        btn_mvvm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Normal:
                startActivity(new Intent(this, NormalActivity.class));
                break;
            case R.id.btn_mvc:
                startActivity(new Intent(this, MVCActivity.class));
                break;
            case R.id.btn_mvp:
                startActivity(new Intent(this, MVPActivity.class));
                break;
            case R.id.btn_databinding:
                startActivity(new Intent(this, DataBindingActivity.class));
                break;
            case R.id.btn_mvvm:
                startActivity(new Intent(this, MVVMActivity.class));
                break;
        }
    }
}
