package com.wjy.mdemo.databinding;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.wjy.mdemo.R;
import com.wjy.mdemo.bean.UserInfo;

public class DataBindingActivity extends AppCompatActivity {

    ActivityDataBindingBinding binding;
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_data_binding);
        userInfo = new UserInfo();
        userInfo.setName("张三");
        userInfo.setAge(25);
        binding.setUserInfo(userInfo);
        binding.setActivity(this);

//        binding.tvData.setText("");//直接使用，避免繁琐的findViewById
    }

    public void onclick(View view){
        Toast.makeText(this,"点击了按钮",Toast.LENGTH_SHORT).show();
        int age = userInfo.getAge();
        userInfo.setAge(age+1);
//        binding.setUserInfo(userInfo);
    }
}
