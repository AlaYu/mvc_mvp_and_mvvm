package com.wjy.mdemo.mvc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wjy.mdemo.R;
import com.wjy.mdemo.bean.UserInfo;
import com.wjy.mdemo.callback.MCallBack;

/**
 * created by WangJinyong
 */
public class MVCActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_name;//输入要查询的用户名
    private Button btn_search;//查询按钮
    private TextView tv_result;//显示查询结果
    private MVCModel mvcModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvc);
        initView();
        mvcModel = new MVCModel();//初始化
    }

    //初始化View
    private void initView(){
        et_name = findViewById(R.id.et_name);
        btn_search = findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        tv_result = findViewById(R.id.tv_result);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search://查询用户信息
                mvcModel.getUserInfo(getInputData(), new MCallBack() {
                    @Override
                    public void onSuccess(UserInfo userInfo) {
                        showSuccess(userInfo);//成功
                    }

                    @Override
                    public void onFailed() {
                        showFailed();//失败
                    }
                });
                break;
        }
    }

    /**
     * 获取用户输入的信息,直接返回输入框的信息就可以了
     */
    private String getInputData(){
        return et_name.getText().toString();
    }

    /**
     * 获取数据成功
     */
    private void showSuccess(UserInfo userInfo){
        tv_result.setText("查询结果：\n用户名："+userInfo.getName()+"\n性别："+userInfo.getSex()+"\n年龄："+userInfo.getAge());
    }

    /**
     * 获取数据失败
     */
    private void showFailed(){
        tv_result.setText("查询结果：\n获取数据失败！");
    }
}
